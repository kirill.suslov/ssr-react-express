import express from 'express';
import React from 'react';
import ReactDOMServer from 'react-dom/server';

import App from '../client/App';

const app = express();
const PORT = process.env.PORT || 3006;

app.get('*', (req, res)=> {
  const app = ReactDOMServer.renderToString(<App pathname={req.url}/>);
  res.send(app);
});

app.listen(PORT, ()=> {
  console.log('😎 Server is listening on port ' + PORT);
});
