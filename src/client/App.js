import React from 'react';
import ReactDOM from 'react-dom';

const PAGES = {
    '/': "Home",
    '/blog': "Blog",
    '/contact': "Contact me"
};

class App extends React.Component {
  render(){
    let page = PAGES[this.props.pathname] || "404"
    return (<h1>{page}</h1>);
  }
}

export default App;
